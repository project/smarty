This directory will be parsed for Smarty plugins.

Addtionally, if a 'plugins' or 'configs' directory exists within the main template engine directory or any Smarty theme's directory it will be parsed accordingly.

Refer to Smarty documentation for details on plugins and the configs directory.

See www.drupal.org/project/smarty for more documentation.
